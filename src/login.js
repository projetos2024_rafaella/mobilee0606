import React, { useState } from "react";
import { View, Text, TextInput, TouchableOpacity, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import sheets from "./axios/axios";

const Login = () => { // Define o componente de função Login
  const [verSenha, setVerSenha] = useState(false); // Define o estado para controlar se a senha está visível ou não
  const [email, setEmail] = useState(""); // Define o estado para armazenar o email inserido pelo usuário
  const [senha, setSenha] = useState(""); // Define o estado para armazenar a senha inserida pelo usuário
  const navigation = useNavigation(); // Obtém o objeto de navegação usando o hook useNavigation

 

  return (
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <View
        style={{
          flex: 1,
          marginHorizontal: 22,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ position: 'absolute', top: 80, left: 0, }}>
          <Ionicons name="chevron-back-outline" size={40} color="black" />
        </TouchableOpacity>

        <View style={{ marginVertical: 22 }}>
          <Text
            style={{
              fontSize: 22,
              fontWeight: "bold",
              marginVertical: 12,
              color: "black",
            }}
          >
            Faça login na sua conta!
          </Text>
        </View>

        <View style={{ marginBottom: 12, width: "100%" }}>
          <Text style={{ fontSize: 16, fontWeight: 400, marginVertical: 8 }}>
            Email:
          </Text>
          <View
            style={{
              width: "100%",
              height: 48,
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 8,
              flexDirection: "row",
              alignItems: "center",
              paddingLeft: 22,
            }}
          >
            <TextInput
              placeholder="Insira seu email"
              placeholderTextColor="black"
              keyboardType="email-address"
              style={{ flex: 1 }}
              onChangeText={setEmail}
              value={email}
            />
          </View>
        </View>

        <View style={{ marginBottom: 12, width: "100%" }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 400,
              marginVertical: 8,
            }}
          >
            Senha:
          </Text>
          <View
            style={{
              flexDirection: "row",
              width: "100%",
              height: 48,
              borderColor: "black",
              borderWidth: 1,
              borderRadius: 8,
              alignItems: "center",
              paddingLeft: 22,
            }}
          >
            <TextInput
              placeholder="Insira sua senha"
              placeholderTextColor="black"
              secureTextEntry={verSenha}
              style={{ flex: 1 }}
              onChangeText={setSenha}
              value={senha}
            />

            <TouchableOpacity
              onPress={() => setVerSenha(!verSenha)}
              style={{ paddingHorizontal: 12 }}
            >
              {verSenha ? (
                <Ionicons name="eye" size={24} color="black" />
              ) : (
                <Ionicons name="eye-off" size={24} color="black" />
              )}
            </TouchableOpacity>
          </View>
        </View>

        <View
          style={{
            marginBottom: 12,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            onPress={() => navigation.navigate("Fantasias")}
            style={{
              backgroundColor: "#7435F0",
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 5,
              margin: 20,
              height: 50,
              width: 100,
              shadowColor: "black",
              shadowRadius: 10,
            }}
          >
            <Text style={{ color: "white", fontWeight: "bold" }}>Entrar</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate("Escolha")}>
            <Text style={{ textDecorationLine: 'underline' }}>
              Esqueceu sua senha?
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Login;
