import React from "react";
import Home from "./src/home";
import Login from "./src/login";
import Cadastro from "./src/cadastro";
import Fantasias from "./src/fantasias";
import Detalhes from "./src/fantasiasDetails"
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={Home}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Cadastro"
          component={Cadastro}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Fantasias"
          component={Fantasias}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Detalhes"
          component={Detalhes}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

