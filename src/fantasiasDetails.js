import React, { useState } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ScrollView,
  Image,
  Text,
  Dimensions,
  Button,
  Modal,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { TextInput } from "react-native-gesture-handler";
import ModalReserva from "./components/modalReserva";

function Detalhes({ route }) {
  const { fantasias } = route.params;
  const navigation = useNavigation();
  const [showModal, setShowModal] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        {/* Estilizando o botão de voltar */}
        <TouchableOpacity
          style={styles.backButton}
          onPress={() => navigation.goBack()}
        >
          <Text style={styles.backButtonText}>Voltar</Text>
        </TouchableOpacity>
      </View>
      <Image style={styles.image} source={fantasias.image} />
      <View style={styles.textContainer}>
        <Text style={styles.text}>Nome: {fantasias.name}</Text>
        <Text style={styles.text}>Preço: {fantasias.preço}</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() => setShowModal(true)}
      >
        <Text style={styles.buttonText}>Reservar</Text>
      </TouchableOpacity>

      <Modal visible={showModal} animationType="slide" transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.modalContainer}>
            <ModalReserva></ModalReserva>
            <TouchableOpacity
              style={styles.backButton}
              onPress={() => setShowModal(false)}
            >
              <Text>Reservar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  backButton: {
    // Estilos para o botão de voltar
    backgroundColor: "#7435F0", // Cor de fundo do botão
    paddingVertical: 15, // Preenchimento vertical
    paddingHorizontal: 30, // Preenchimento horizontal
    borderRadius: 5, // Raio da borda
    marginBottom: 20, // Margem inferior
    width: "80%", // Largura do botão
    position: "absolute", // Define a posição do botão como absoluta
    bottom: 60, // Define a distância do final da tela
  },

  container: {
    flex: 1,
    alignItems: "center",
    paddingBottom: 100,
  },
  header: {
    position: "absolute",
    top: 60,
    left: 20,
  },
  // Estilos para o botão de voltar
  backButton: {
    backgroundColor: "#7435F0",
    paddingVertical: 14,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  backButtonText: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "#7435F0",
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 5,
    marginBottom: 20,
    width: "80%",
    position: "absolute",
    bottom: 20,
  },
  buttonText: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },
  textContainer: {
    alignItems: "center",
    marginTop: 100,
  },
  text: {
    marginBottom: 10,
    top: 105,
    fontSize: 20,
    fontWeight: "bold",
  },
  image: {
    width: 300,
    height: 400,
    top: 130,
  },
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  modalContainer: {
    backgroundColor: "#084d6e",
    borderRadius: 10,
    padding: 20,
    width: "80%",
    maxHeight: "70%",
    justifyContent: "center",
    alignItems: "center",
  },
});

export default Detalhes;
