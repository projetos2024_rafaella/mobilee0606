import React, { useState } from "react";
import { View, TouchableOpacity, Text, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import DateTimePicker from "./datePicker";

const ModalReserva = () => {
    const [reserva, setReserva] = useState({
        ID_produto: null,
        ID_cliente: null,
        Data_da_reserva: 'Data da reserva',
        Data_de_retirada: 'Data da retirada',
        Data_de_devolucao: 'Data da devolução',
        Status_da_reserva: null,
    });

    const navigation = useNavigation();

    const reservarFantasia = () => {
        // Aqui você pode implementar a lógica para reservar a fantasia
        // Por exemplo, enviar os dados da reserva para o servidor
        
        // Após a reserva, você pode fechar o modal, se necessário
        navigation.goBack(); // Fecha o modal
    };

    return (
        <View>
            {/* Adicionando o componente DateTimePicker */}
            <DateTimePicker type={'date'} buttonTitle={reserva.Data_da_reserva} dateKey={'Data_da_reserva'} setReserva={setReserva}/>
            <DateTimePicker type={'date'} buttonTitle={reserva.Data_de_retirada} dateKey={'Data_de_retirada'} setReserva={setReserva} />
            <DateTimePicker type={'date'} buttonTitle={reserva.Data_de_devolucao} dateKey={'Data_de_devolucao'} setReserva={setReserva}/>

            {/* Botão para reservar */}
            <TouchableOpacity style={styles.button} onPress={reservarFantasia}>
                <Text style={styles.buttonText}>Reservar</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: "#7435F0",
        paddingVertical: 15,
        paddingHorizontal: 30,
        borderRadius: 5,
        marginTop: 20,
    },
    buttonText: {
        color: "white",
        fontSize: 20,
        fontWeight: "bold",
    },
});

export default ModalReserva;
