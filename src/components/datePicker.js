import React, { useState } from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";


const DateTimePicket = ({ type, buttonTitle, dateKey, setReserva }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    if (type === "time") {
      const hour = date.getHours();
      const minute = date.getMinutes();

      const formattedTime = `${hour}:${minute}`;

      setReserva((prevState) => ({
        ...prevState,
        [dateKey]: formattedTime,
      }));
    } else {
      const formattedDate = date.toISOString().split('T')[0];
      setReserva((prevState) => ({
        ...prevState,
        [dateKey]: formattedDate,
      }));
    }
    hideDatePicker();
  };

  return (
    <View>
      {/* O título do botão deve ser uma string */}
      <Button title={buttonTitle} onPress={showDatePicker} color="black" />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  );
};

export default DateTimePicket;
