import axios from "axios"; // Importa o pacote axios para lidar com requisições HTTP

const api = axios.create({ // Cria uma instância do axios com configurações personalizadas
    baseURL: "http://10.89.234.170:5000/api/", // Define a URL base para todas as requisições
    headers: { // Define os cabeçalhos padrão para todas as requisições
        'accept': 'application/json' // Define o cabeçalho 'accept' como 'application/json'
    },
});

const sheets = { // Define um objeto chamado sheets
   
};

export default sheets; // Exporta o objeto sheets para ser utilizado em outros arquivos

